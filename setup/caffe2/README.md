# Caffe 2


NOTE: You'll have to update cmake_config.cmake with some user specific paths.

```bash
mkdir build
cmake -C ~/trinity/utils/setup/caffe2/cmake_config.cmake -DCMAKE_INSTALL_PREFIX:PATH=/home/achald/.local/caffe2/ ..
```

For Cuda 8:

Note that this requires a conda installation that's also built against cuda 8.
```bash
module load cuda-8.0
export LD_LIBRARY_PATH=/home/software/cudnn-7.0.5-cuda8/lib64/:"${LD_LIBRARY_PATH}"
cmake -C ~/trinity/utils/setup/caffe2/cmake_config_cuda8.cmake -DCMAKE_INSTALL_PREFIX:PATH=/home/achald/.local/caffe2-cuda8/ ..
```
