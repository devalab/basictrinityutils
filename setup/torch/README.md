# Torch setup on Trinity

Author: Achal Dave

For the most part, you can simply follow the install script provided by torch,
provided you source the following files before compiling:

    source /path_to_trinity_repo/setup/caffe/caffe_env
    source /path_to_trinity_repo/setup/python/python_env
    source /path_to_trinity_repo/setup/torch/torch_env

It's probably not necessary to source all of them, but it should be sufficient.

*NOTE*: The `torch_env` file should _not be sourced_ in your .bashrc - it is
required for installing torch, but causes issues with GCC because it modifies
the `LIBRARY_PATH` variable. You should source it only while installing torch,
but should not source it when using torch.
