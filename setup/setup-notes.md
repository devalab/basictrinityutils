This is a list of things I (achald) had to do to setup various software on our
internal cluster, Trinity. No guarantee it'll work for you.

# Vim

configure:

    LD_LIBRARY_PATH="/usr/lib64/perl5/CORE:${LD_LIBRARY_PATH}" \
    CFLAGS="-D_GNU_SOURCE -I/usr/lib64/perl5/CORE" \
    LDFLAGS="-L/usr/lib64/perl5/CORE -lperl" \
    ./configure --with-features=huge \
    --enable-multibyte \
    --enable-rubyinterp \
    --enable-pythoninterp \
    --with-python-config-dir=/opt/python27/lib/python2.7/config \
    --enable-perlinterp \
    --enable-luainterp \
    --enable-cscope \
    --prefix="/home/achald/.local/vim-7.4"

make:

    make -j8 && make install

etc:

Add to .zshrc/.bashrc:
    export LD_LIBRARY_PATH="/usr/lib64/perl5/CORE:${LD_LIBRARY_PATH}"

# Boost

    ./bootstrap.sh --prefix="/home/software/boost-1.59.0" \
        --with-python="/opt/python27/bin/python2"
    ./bjam

# scikit-image

In setup.py, I had to comment out a line that tried to install cython 0.21.0.
I'm not exactly sure what was going on...

# Libsodium
I had to pass '-m no-avx' to avoid 'no such instruction' errors:

    CFLAGS="-mno-avx" CXXFLAGS="-mno-avx" ./configure \
        --prefix="$HOME/.local/libsodium-1.0.10"
    make -j4

# Zeromq

    LD_LIBRARY_PATH="$HOME/.local/libsodium-1.0.10/lib" \
        LDFLAGS="-L$HOME/.local/libsodium-1.0.10/lib -lsodium" \
        sodium_LIBS="$HOME/.local/libsodium-1.0.10/lib" \
        sodium_CFLAGS="-I$HOME/.local/libsodium-1.0.10/include" \
        ./configure --prefix="$HOME/.local/zmq-4.1.4"

# Torch image package
I had to install the latest libjpegturbo to ~/.local, and then added

    -DJPEG_LIBRARY:PATH="/home/achald/.local/libjpeg-turbo-1.5.0/lib/libjpeg.so"
    -DJPEG_INCLUDE_DIR:PATH="/home/achald/.local/libjpeg-turbo-1.5.0/include/"

to the cmake build command in the rockspec.

# Lua hdf5
````bash
export PATH="$PATH:/home/software/hdf5-1.8.16"
git clone https://github.com/deepmind/torch-hdf5.git
cd torch-hdf5
luarocks make
````

# DenseFlow

````bash
CC=/opt/gcc/4.9.2/bin/gcc \
   CXX=/opt/gcc/4.9.2/bin/g++ \
   OpenCV_DIR=/home/achald/.local/opencv-3.1.0/ \
   cmake .. \
   -DLIBZIP_LIBRARY=/home/achald/.local/libzip-1.1.3/lib/libzip.so \
   -DLIBZIP_INCLUDE_DIR_ZIPCONF=/home/achald/.local/libzip-1.1.3/lib/libzip/include/ \
   -DLIBZIP_INCLUDE_DIR_ZIP=/home/achald/.local/libzip-1.1.3/include \
   -DBoost_NO_BOOST_CMAKE=TRUE
````

# OpenCV

```bash
cmake -D CMAKE_BUILD_TYPE=RELEASE \
    -D CMAKE_INSTALL_PREFIX=/usr/local \
    -D CUDA_TOOKIT_ROOT_DIR=/opt/cuda/ \
    -D WITH_CUDA=ON \
    -D ENABLE_FAST_MATH=1 \
    -D CUDA_FAST_MATH=1 \
    -D WITH_CUBLAS=1 \
    -D INSTALL_PYTHON_EXAMPLES=ON \
    -D OPENCV_EXTRA_MODULES_PATH=../../opencv_contrib-3.1.0/modules \
    -D BUILD_EXAMPLES=ON ..
````
