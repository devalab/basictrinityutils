# Caffe setup on Trinity

Author: Achal Dave

# Description

Compiling Caffe, in general, is slightly tricky because Caffe has a number of
dependencies. SCS installed a number of the necessary dependencies on Trinity,
but some were either missing or were older versions, and so I (@achald)
installed them locally in /home/software. For the compiler and linker to locate
these, you need to set up a few environment variables and create a
Makefile.config for Caffe. The two files in this directory take care of that.

# Setup instructions

1. Source the `caffe_env` script in this directory. You can also add this to
   your `~/.bashrc` (or equivalent for your shell):

    ```bash
    source /path_to_trinity_repo/setup/python/python_env
    source /path_to_trinity_repo/setup/caffe/caffe_env
    export GLOG_logdir="/data/${USER}/glog"
    ```

    The last line will tell Glog to store your log files in a user specific
    directory. If you don't specify this, `/tmp` will be flush with log files
    from everyone, and the symlinks the glog creates will be overwritten by
    other users.

2. Copy the `Makefile.config` (or `Makefile.config.with_cudnn` if you want to
   build Caffe with cudnn) in this directory to the Caffe source directory.

3. Add the following to your `~/.bashrc` (or equivalent for your shell). This
   will allow CUDA to be visible in your system environment.

    ```bash
    module load cuda-8.0
    ```

4. Compile Caffe:

    ```bash
    make -j8
    make pycaffe
    ```

Currently, a bunch of warnings are output during compilation, which, as far as I
can tell, you can safely ignore.
