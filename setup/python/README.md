# Python

Just add the following line to your .bashrc

    source /path_to_trinity_repo/setup/python/python_env

## Jupyter notebook (formerly ipython notebook)

To use Jupyter notebook (formerly known as ipython notebook), we first have to
take some security precautions. Running a Jupyter notebook on 0.0.0.0 will allow
any person in the world who has a link to your notebook to run arbitrary
commands on the server! To avoid this, we will password-protect the notebooks.

The following steps are taken from the documentation
[here](http://jupyter-notebook.readthedocs.io/en/latest/public_server.html#securing-a-notebook-server)

1. Generate a jupyter configuration file if you don't already have one. This
   should be located at `~/.jupyter/jupyter_notebook_config.py`.

    ```
    jupyter notebook --generate-config
    ```
2. You will store a hashed password in the configuration file generated above.
   To generate the hash of your password, run the following in python:

    ```
    In [1]: from IPython.lib import passwd
    In [2]: passwd()
    Enter password:
    Verify password:
    Out[2]: 'sha1:YOUR_GENERATED_HASH'
    ```
3. Add the hashed password to your notebook configuration file. By default, this
   is `~/.jupyter/jupyter_notebook_config.py`; open it, and add the `sha1` hash
   from above as follows:

    ```
    c.NotebookApp.password = u'sha1:YOUR_GENERATED_HASH'
    ```

4. Add an HTTPS certificate file and .PEM key to enable HTTPS access, so you are
   not sending passwords over plain text. Add the following lines to your config
   file:

   ```
   c.NotebookApp.certfile = u'/data/achald/ssl-cert/devalab.pem'
   c.NotebookApp.keyfile = u'/data/achald/ssl-cert/devalab.key'
   ```

Now, you're ready to launch jupyter notebooks on the head node! To do this, run
the following command on the head node, replacing the `$MYPORT` variable with
your assigned port (see doc titled "Trinity: Serving webpages").

    jupyter notebook --no-browser --port $MYPORT --ip 0.0.0.0

However, it is not advisable to run notebooks on head node. Run the above on
a compute node instead, say `compute-X-X`, and forward the port to the trinity
head node by running the following on the head node:

```
ssh -L $MYPORT:localhost:$MYPORT -N -o GatewayPorts=yes compute-X-X
```

**Warning**: Ensure that you always visit the `https://` link, and not the
`http://` link.

When you visit the `https://` link, you may get a complaint about an unrecognized
certificate. This is because our certificate is not verified by a Certificate
Authority. If you would like to verify that you are on the correct website, you
can ensure that the certificate matches the following:

```
-----BEGIN CERTIFICATE-----
MIICnDCCAgWgAwIBAgIJAKh0smNjdhoUMA0GCSqGSIb3DQEBCwUAMGcxCzAJBgNV
BAYTAlVTMQswCQYDVQQIDAJQQTETMBEGA1UEBwwKUGl0dHNidXJnaDEQMA4GA1UE
CgwHRGV2YUxhYjEkMCIGCSqGSIb3DQEJARYVYWNoYWxkQGFuZHJldy5jbXUuZWR1
MB4XDTE3MDMyNjE5NDQxMVoXDTE4MDMyNjE5NDQxMVowZzELMAkGA1UEBhMCVVMx
CzAJBgNVBAgMAlBBMRMwEQYDVQQHDApQaXR0c2J1cmdoMRAwDgYDVQQKDAdEZXZh
TGFiMSQwIgYJKoZIhvcNAQkBFhVhY2hhbGRAYW5kcmV3LmNtdS5lZHUwgZ8wDQYJ
KoZIhvcNAQEBBQADgY0AMIGJAoGBALwNYuKJ+IIUoMA9IvRijsNykmnyeckyqMPm
7Z8HO7loJe5rQU4F/a55G7WtGgl/lf9uJ6iS/Evhavf7gJZmVq5JrNIgiAE+m6Y+
pOu96LTg+jOOnKD3SyNrv34esy0ZTeRHVm31DpgWDNmzyov18dimyQ8x0Lrmu9EP
E59l68CvAgMBAAGjUDBOMB0GA1UdDgQWBBQ2n8DlZ4dlhG6NeHhqkNhsKJ7STzAf
BgNVHSMEGDAWgBQ2n8DlZ4dlhG6NeHhqkNhsKJ7STzAMBgNVHRMEBTADAQH/MA0G
CSqGSIb3DQEBCwUAA4GBAH9pJJRGHXsrD+Llbflq6tJASTgJMwDIQj9B7m1MSJ6P
1bcVj5+5Q7YhL9+31nFgSN6Ym1JcobjYj8VnApp439GMvCNoTsNeeEvTm7s1Wq4+
SIPV7vaC6Xw49/5pjKRHdfDYtM4tH0giPZKfmwWO+10TFPEKnqQN9BtpFADqAuOI
-----END CERTIFICATE-----
```
