import argparse
import collections
import logging
from pathlib import Path


def to_raw_size(x):
    multipliers = {'T': 1e12, 'G': 1e9, 'M': 1e6, 'K': 1e3}
    if x[-1] in multipliers:
        return float(x[:-1]) * multipliers[x[-1]]
    else:
        return float(x)


def to_human(x):
    if x > 1e12:
        x = f'{x / 1e12:.2f}T'
    elif x > 1e9:
        x = f'{x / 1e9:.0f}G'
    elif x > 1e6:
        x = f'{x / 1e6:.0f}M'
    elif x > 1e3:
        x = f'{x / 1e3:.0f}K'
    else:
        x = f'{x:.0f}'
    return x


def main():
    # Use first line of file docstring as description if it exists.
    parser = argparse.ArgumentParser(
        description=__doc__.split('\n')[0] if __doc__ else '',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('sizes_dir', type=Path)
    parser.add_argument('output_parsed')

    args = parser.parse_args()

    size_paths = list(args.sizes_dir.glob('compute-*-sizes.txt'))
    user_usages = collections.defaultdict(list)
    for size_path in size_paths:
        node = size_path.name.split('-sizes')[0]
        with open(size_path, 'r') as f:
            for line in f:
                # Lines are of the form {size}\t/{root}/{subdir}
                size, path = line.strip().split('\t')
                _, root, user = path.split('/')
                user_usages[user].append((node, root, size, to_raw_size(size)))

    output_html = """
    <html>
    <head>
    <style type='text/css'>
    table {{
        display: inline-block;
        vertical-align: top;
        border: solid 1px black;
        margin: 10px 10px;
    }}
    td {{
       min-width: 100px;
       overflow: auto;
    }}
    thead {{
        font-weight: 900;
    }}
    </style>
    <body>
    {table}
    </body>
    </html>"""
    user_sizes_container = """<table>
    <thead>
    <tr><td colspan='3'>User: {user}</td></tr>
    <tr>
    <td>Node</td><td>Dir</td><td>Usage</td>
    </tr>
    </thead>
    {size_rows}
    </table>"""
    user_size_template = (
        "<tr><td>{node}</td><td>{dir}</td><td>{size}</td></tr>")

    tables = ""
    total_usages = {
        user: sum(x[3] for x in v)
        for user, v in user_usages.items()
    }
    user_usages_list = sorted(user_usages.items(),
                              key=lambda x: total_usages[x[0]], reverse=True)
    for user, sizes in user_usages_list:
        size_rows = ""
        sorted_sizes = sorted(sizes, key=lambda x: x[3], reverse=True)
        sorted_sizes.insert(0, ("Total", "-", to_human(total_usages[user])))
        size_rows = "\n".join([
            user_size_template.format(node=x[0], dir=x[1], size=x[2])
            for x in sorted_sizes
        ])
        tables += user_sizes_container.format(user=user, size_rows=size_rows)
    output_html = output_html.format(table=tables)
    with open(args.output_parsed, 'w') as f:
        f.write(output_html)


if __name__ == "__main__":
    main()
