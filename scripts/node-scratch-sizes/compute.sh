#!/bin/bash

if [[ "$#" != 1 ]]  ; then
    echo "Usage: $0 <output-dir>"
    exit 1
fi

NODE_LIST=../../../gpumon/node-list.txt
if [[ ! -e ${NODE_LIST} ]] ; then
    echo "Could not find nodes list at ${NODE_LIST}"
    exit 1
fi
NODE_LIST=$(readlink -e ${NODE_LIST})

DIR=$(date +'%m-%d-%y')
TMPDIR=${DIR}/tmp/

if [[ -d "${DIR}" ]] ; then
    echo "${DIR} already exists"
    exit 1
fi

mkdir -p ${DIR} ${TMPDIR}
pushd ${TMPDIR}

function check_sizes() {
    node=$1
    echo "${node}"
    ssh ${node} 'sudo du -sh /scratch/*' >> ${node}-sizes.txt
    echo "Processed ${node} /scratch"
    ssh ${node} 'sudo du -sh /ssd0/*' >> ${node}-sizes.txt
    echo "Processed ${node} /ssd0"
    ssh ${node} 'sudo du -sh /ssd1/*' >> ${node}-sizes.txt
    echo "Processed ${node} /ssd1"
}

export -f check_sizes
# Nasty xargs invocation required for passing a function to xargs:
# https://stackoverflow.com/a/11003457
cat ${NODE_LIST} | xargs -n1 -P8 -I{} bash -c 'check_sizes "$@"' _ {}

touch all-sizes.txt
echo "Last update: $(date +'%m-%d-%y %H:%M:%S')" >> all-sizes.txt
cat ${NODE_LIST} | while read node ; do
    echo $node >> all-sizes.txt
    sort -h -r ${node}-sizes.txt >> all-sizes.txt
    break
done

popd
mv -f ${TMPDIR}/* ${DIR}
python parse.py ${DIR} ${DIR}/index.html
