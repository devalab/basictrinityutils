import time, subprocess
import random

def check():
    info = {}
    processes = {}
    subprocess.call(['./check_gpus.sh'])
    ps = False
    current_node = None
    current_gpu = -1
    for i, line in enumerate(open('gpus_info.txt')):
        if line.startswith('compute-0-'):
            current_node = int(line.split('-')[2].strip())
            current_gpu = -1
            info[current_node] = []
            processes[current_node] = {}
            ps = False
            continue
        if 'used_gpu_memory' in line:
            current_gpu += 1
            info[current_node].append([])
            continue
        if 'No devices were found' in line:
            # Strange issue where nvidia-smi doesn't recognize that a GPU
            # exists.
            current_gpu = current_gpu + 1
            info[current_node].append([])
            info[current_node][current_gpu].append(
                '00000, ERROR WITH NVIDIA-SMI, 99999 MiB')
            processes[current_node][0] = 'root'
            continue
        if 'CMD' in line:
            ps = True
            continue
        if ps:
            line = line.split()
            name = line[0]
            pid = int(line[1])
            if name != 'root':
                processes[current_node][pid] = name
        elif current_node >= 0 and current_gpu >= 0:
            info[current_node][current_gpu].append(line)
    return info, processes

def writeHTML(info, processes):
    PUSHEEN = random.choice([1, 5])
    f = open('gpus.html', 'w')
    f.write('<head><link rel="stylesheet" type="text/css" href="gpu.css">\n<title>Trinity GPUs </title></head>\n')
    f.write('<h1> Trinity GPUs </h1>\n')

    if PUSHEEN == 1:
        f.write('<h3>Pusheen said work hard!! :D</h3>\n')
        f.write('<img src="pusheen.gif" alt="Pusheen">\n')
    elif PUSHEEN == 5:
        f.write('<img src="https://media.giphy.com/media/lbQc9gXCOBEL6/giphy.gif" alt="Tuzki">\n')

    t = time.localtime()
    timeString = time.strftime("%m/%d/%Y: %H:%M:%S", t)

    f.write('<h1>%s</h1>' % timeString)

    for nodeID in sorted(info.keys()):
        f.write('<h3>node %d</h3>\n' % nodeID)
        f.write('<table><tr><td>GPU ID</td><td>Usage</td><td>User</td><td>Memory</td><td>Pid</td><td>Cmd</td></tr>\n')

        for gpuID in range(len(info[nodeID])):
            memAll = 0
            for line in info[nodeID][gpuID]:
                mem = line.split(',')[2]
                mem = int(mem.split()[0])
                memAll += mem
            f.write('<tr><td rowspan="%d"> %d </td>' % (len(info[nodeID][gpuID]), gpuID))
            f.write('<td rowspan="%d"> %d/12287 MiB </td>' % (len(info[nodeID][gpuID]), memAll))
            for line in info[nodeID][gpuID]:
                line = line.split(',')
                pid = int(line[0])
                cmd = line[1]
                mem = line[2]
                f.write('<td>%s</td><td>%s</td><td>%d</td><td>%s</td></tr>' % (processes[nodeID][pid], mem, pid, cmd))
                f.write('<tr>')
        f.write('</table>')
    f.close()

if __name__ == '__main__':
    # while True:
    info, processes = check()
    writeHTML(info, processes)
    #     time.sleep(10) # 10 sec
