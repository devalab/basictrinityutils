#!/bin/bash

unset SLURM_JOB_ID
unset SLURM_JOBID
echo "GPU INFO" > gpus_info.txt
for node in "compute-0-11" "compute-0-12" "compute-0-14" "compute-0-15" "compute-0-17" "compute-0-20" "compute-0-21" ; do
    echo $node | tee -a gpus_info.txt
    echo "" >> gpus_info.txt
    timeout -s 9 300s srun --mem 100 -c 1 -w $node timeout -s 9 200s bash -c \
        'unset GPU_DEVICE_ORDINAL; unset CUDA_VISIBLE_DEVICES ; \
        num_watchers="$(ps aux | grep nvidia-smi | wc -l)" ; \
        if [[ $num_watchers -gt 30 ]] ; then \
            echo "Too many nvidia-smi processes: $num_watchers" >&2 ; \
            exit 1 ; \
        fi ; \
        timeout -s 9 30s nvidia-smi --query-gpu=index,memory.free,memory.used --format=csv; \
        for i in {0..3}; do \
            timeout -s 9 30s nvidia-smi --id=$i --query-compute-apps=pid,name,used_memory  --format=csv; \
        done; \
        timeout 30s ps -Ao uname:20,pid,time,cmd' >> gpus_info.txt
done
